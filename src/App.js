import React, { Component } from "react";
import "./App.css";

class App extends Component {
  state = {
    tasks: [],
    inputText: "",
    filter: 'all'
  };

  handleChange = e => {
    this.setState({
      inputText: e.target.value
    });
  };

  handleClick = () => {
    if (this.state.inputText === "") return;
    this.setState({
      tasks: [
        { content: this.state.inputText, isChecked: false },
        ...this.state.tasks
      ],
      inputText: ""
    });
  };

  onClickCheckbox = (e, index) => {
    const tasks = this.state.tasks.map((task, i) => {
      if (i === index) {
        return { ...task, isChecked: e.target.checked };
      }
      return task;
    });
    this.setState({ tasks });
  };

  checkOrUncheckAll = (value) => {
    const tasks = this.state.tasks.map((task) => {
      return { ...task, isChecked: value };
    });
    this.setState({ tasks });
  };

  showCompleted = () => {
    this.setState({filter: 'completed'});
  };

  showUncompleted = () => {
    this.setState({ filter: 'uncompleted' })
  }

  showAll = () => {
    this.setState({filter: 'all'});
  }
  
  render() {
    return (
      <div>
        <h1>To Do List</h1>
        <div>
          <input
            type="text"
            onChange={this.handleChange}
            value={this.state.inputText}
          />
        </div>
        <div>
          {this.state.tasks
          .filter(task => (this.state.filter === 'completed') ? task.isChecked : this.state.filter === 'uncompleted' ? !task.isChecked : true)
          .map((task, index) => (
            <Task
              index={index}
              key={index}
              content={task.content}
              isChecked={task.isChecked}
              onClickCheckbox={this.onClickCheckbox}
            />
          ))}
        </div>
        <div>
          <span />
          <button className="btn-primary" onClick={this.handleClick}>create</button>
          <button className="btn-primary" onClick={() => this.checkOrUncheckAll(true)}>Check all</button>                                      
          <button className="btn-primary" onClick={() => this.checkOrUncheckAll(false)}>Uncheck all</button>
          <button className="btn-primary" onClick={this.showAll}>Show All</button>
          <button className="btn-primary" onClick={this.showCompleted}>Show Completed</button> 
          <button className="btn-primary" onClick={this.showUncompleted}>Show Uncompleted</button>                             
        </div>
      </div>
    );
  }
}

const Task = props => {
  return (
    <div className="task">
      <input
        type="checkbox"
        onChange={e => props.onClickCheckbox(e, props.index)}
        checked={props.isChecked}
      />
      <span>{props.content}</span>
    </div>
  );
};

export default App;
